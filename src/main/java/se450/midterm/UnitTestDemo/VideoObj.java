package se450.midterm.UnitTestDemo;

public class VideoObj {
	private String title; 
	private int year;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	private String director;
	public VideoObj(String title, int year, String director){ 
		this.title = title; 		
		this.year = year; 
		if(director == null || director.trim().equals("")) 
			throw new IllegalArgumentException("Director"); 
		this.director = director; 
	} 
}

