package se450.midterm.StaticFactoryDemo;

class Circle implements IShape {
	public Circle(int radius){ } 
} 
class Square implements IShape { 
	public Square(int sideLength){ } 
} 
class Rectangle implements IShape { 
	public Rectangle(int width, int height){ } 
}

public class ShapeFactory {
	static IShape createCircle(int radius){
		return new Circle(radius);
	}
	
	static IShape createSquare(int sideLength) {
		return new Square(sideLength);
	}
	
	static IShape createRectangle(int width, int height){
		return new Rectangle(width, height);
	}
}