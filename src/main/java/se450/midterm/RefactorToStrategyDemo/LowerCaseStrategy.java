package se450.midterm.RefactorToStrategyDemo;

public class LowerCaseStrategy implements ITextFormatter {

	@Override
	public String formatText(String input) {
		return input.toLowerCase();
	}

}
