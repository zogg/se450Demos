package se450.midterm.RefactorToStrategyDemo;

public class UpperCaseStrategy implements ITextFormatter {

	@Override
	public String formatText(String input) {
		return input.toUpperCase();
	}

}
