package se450.midterm.RefactorToStrategyDemo;

public interface ITextFormatter {
	String formatText(String input);
}
