package se450.week3.StaticFactoryDemo;

public interface IShape {
	String getString();
}
