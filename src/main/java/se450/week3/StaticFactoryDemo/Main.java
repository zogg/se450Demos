package se450.week3.StaticFactoryDemo;

public class Main {

	public static void main(String[] args) {
		String shapeType = "triangle";
		
		IShape shape = ShapeFactory.createShape(shapeType);
		
		System.out.println(shape.getString());
	}

}
