package se450.week3.StaticFactoryDemo;

import java.security.InvalidParameterException;

public class ShapeFactory {
	public static IShape createShape(String shape){
		IShape createdShape;
		
		if(shape.toLowerCase().equals("circle")){
			createdShape = new Circle();
		}
		else if (shape.toLowerCase().equals("rectangle")){
			createdShape = new Rectangle();
		}
		else if (shape.toLowerCase().equals("triangle")){
			createdShape = new Triangle();
		}
		else{
			throw new InvalidParameterException("Parameter must be the correct shape");
		}
		
		return createdShape;
	}
}
