package se450.week3.RefactorToStrategyDemo;

public class TextConverter {
	String text; 
	ITextFormatter textFormatter;
	
	public TextConverter(String text, int printType) { 
		this.text = text; 
		
		textFormatter = TextFormatterFactory.getStrategy(printType);
	} 
	
	public String convertText(){ 
		return textFormatter.format(text);
	} 
}

class TextFormatterFactory {
	public static ITextFormatter getStrategy(int printType){
		ITextFormatter textFormatter=null;
		
		if(printType == 0) 
			textFormatter = new UpperCaseFormatterStrategy(); 
		else if(printType == 1) 
			textFormatter = new LowerCaseFormatterStrategy();
		else 
			textFormatter = new NormalTextFormatterStrategy();
		
		return textFormatter;
	}
}

class UpperCaseFormatterStrategy implements ITextFormatter {
	public String format(String text){
		return text.toUpperCase();
	}
}

class LowerCaseFormatterStrategy implements ITextFormatter {
	public String format(String text){
		return text.toLowerCase();
	}
}

class NormalTextFormatterStrategy implements ITextFormatter {
	public String format(String text){
		return text;
	}
}