package se450.week3.RefactorToStrategyDemo;

public interface ITextFormatter {
	String format(String text);
}
