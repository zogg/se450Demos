package se450.week3.StratergyPatternDemo.Utilities;

class SubtractionStrategy implements IOperatorStrategy {

	@Override
	public int calculate(int i1, int i2) {
		return i1 - i2;
	}

}
