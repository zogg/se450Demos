package se450.week3.StratergyPatternDemo.Utilities;

// this is a Static Factory class - it CREATES strategy objects.
public class CalculatorStrategyFactory {

	public static IOperatorStrategy getOperatorStrategy(String operator) throws Exception {
		IOperatorStrategy strategy;
		
		switch(operator) {
			case "+":
				strategy = new AdditionStrategy();
				break;
			case "-":
				strategy = new SubtractionStrategy();
				break;
			case "*":
				strategy = new MultiplicationStrategy();
				break;
			default:
				throw new Exception();
		}
		
		return strategy;
	}

}
