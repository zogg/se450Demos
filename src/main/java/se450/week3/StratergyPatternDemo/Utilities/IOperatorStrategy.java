package se450.week3.StratergyPatternDemo.Utilities;

public interface IOperatorStrategy {
	int calculate(int i1, int i2);
}
