package se450.week3.StratergyPatternDemo;

import java.io.*;

import se450.week3.StratergyPatternDemo.Utilities.CalculatorStrategyFactory;
import se450.week3.StratergyPatternDemo.Utilities.IOperatorStrategy;

public class Main {
	public static void main(String[] args) throws Exception{
		System.out.println("Enter operations in the form of \"x + y\" (with spaces)");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input;
		Calculator calculator = new Calculator();
        while(!(input = br.readLine()).toLowerCase().equals("exit")) {
        	String[] inputParts = input.split(" ");
        	int int1 = Integer.parseInt(inputParts[0]);
        	String operator = inputParts[1];
        	int int2 = Integer.parseInt(inputParts[2]);
        	
			// The following line shows how client code uses a static factory. 
			// In this case, we are receiving a strategy object
        	IOperatorStrategy operatorStrategy = CalculatorStrategyFactory.getOperatorStrategy(operator);
        	
			calculator.setFirstNumber(int1);
			calculator.setSecondNumber(int2);
			int result = calculator.calculate(operatorStrategy);
        	
        	System.out.println(result);
        }
	}
}
