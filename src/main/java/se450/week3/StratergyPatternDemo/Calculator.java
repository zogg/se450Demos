package se450.week3.StratergyPatternDemo;

import se450.week3.StratergyPatternDemo.Utilities.IOperatorStrategy;

public class Calculator {
	private int int1;
	private int int2;
	
	public void setFirstNumber(int i){
		int1 = i;
	}
	
	public void setSecondNumber(int i){
		int2 = i;
	}
	
	// This is how client code utilizes the strategy pattern
	public int calculate(IOperatorStrategy strategy){
		return strategy.calculate(int1, int2);
	}
}