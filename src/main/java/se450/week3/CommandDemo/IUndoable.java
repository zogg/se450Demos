package se450.week3.CommandDemo;

public interface IUndoable {
	void undo();
	void redo();
}
