package se450.week3.CommandDemo;

import se450.week3.CommandDemo.ICommand;

public class RedoCommand implements ICommand {

	@Override
	public void run() {
		CommandHistory.redo();
	}

}
