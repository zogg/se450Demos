package se450.week3.CommandDemo;

public class UndoCommand implements ICommand {

	@Override
	public void run() {
		CommandHistory.undo();
	}

}
