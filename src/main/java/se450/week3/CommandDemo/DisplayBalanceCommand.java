package se450.week3.CommandDemo;

import se450.week3.CommandDemo.BankAccount;

public class DisplayBalanceCommand implements ICommand {
	private final BankAccount _bankAccount;
	
	public DisplayBalanceCommand(BankAccount ba){
		_bankAccount = ba;
	}
	
	
	@Override
	public void run() {
		System.out.println(_bankAccount.getBalance());
	}

}
