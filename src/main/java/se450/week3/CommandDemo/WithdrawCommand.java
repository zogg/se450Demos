package se450.week3.CommandDemo;

import se450.week3.CommandDemo.BankAccount;
import se450.week3.CommandDemo.CommandHistory;
import se450.week3.CommandDemo.ICommand;
import se450.week3.CommandDemo.IUndoable;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class WithdrawCommand implements ICommand, IUndoable {

	private final BankAccount _bankAccount;
	private int withdrawalAmount;
	
	public WithdrawCommand(BankAccount ba){
		_bankAccount = ba;
	}
	
	@Override
	public void run() {
		System.out.println("Enter a withdrawal amount: ");
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input;
		try {
			input = br.readLine();
			withdrawalAmount = Integer.parseInt(input);
			_bankAccount.withdraw(withdrawalAmount);
			CommandHistory.add(this);
		} catch(Exception ex) {}
	}

	@Override
	public void undo() {
		_bankAccount.deposit(withdrawalAmount);
	}

	@Override
	public void redo() {
		_bankAccount.withdraw(withdrawalAmount);
	}

}
