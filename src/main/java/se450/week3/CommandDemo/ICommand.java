package se450.week3.CommandDemo;

public interface ICommand {
	void run();
}
