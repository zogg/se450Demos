package se450.week3.CommandDemo;

public class BankAccount {
	private int balance = 0;
	
	public BankAccount(int initialBalance){
		balance = initialBalance;
	}
	
	public void deposit(int depositAmount){
		balance += depositAmount;
	}
	
	public void withdraw(int withdrawAmount){
		balance -= withdrawAmount;
	}
	
	public int getBalance(){
		return balance;
	}
}
