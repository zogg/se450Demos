package se450.week3.CommandDemo;

import se450.week3.CommandDemo.BankAccount;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class DepositCommand implements ICommand, IUndoable {

	private final BankAccount _bankAccount;
	private int depositAmount;
	
	public DepositCommand(BankAccount ba){
		_bankAccount = ba;
	}
	
	@Override
	public void run() {
		System.out.println("Enter a deposit amount: ");
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input;
		try {
			input = br.readLine();
			depositAmount = Integer.parseInt(input);
			_bankAccount.deposit(depositAmount);
			CommandHistory.add(this);
		} catch(Exception ex) {}
	}

	@Override
	public void undo() {
		_bankAccount.withdraw(depositAmount);
	}

	@Override
	public void redo() {
		_bankAccount.deposit(depositAmount);
	}

}
