package se450.week1.EqualsDemo;

/**
 * Created by craig on 7/7/17.
 */
public class StringObject {
    public String str;
    public StringObject(String str){
        this.str = str;
    }
    public String toString(){
        return str;
    }

    public boolean equals(Object o){
        if(!(o instanceof StringObject))
            return false;

        if(o == null)
            return false;

        StringObject so = (StringObject)o;
        return str.equals(so.str);
    }
}
