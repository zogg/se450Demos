package se450.week1.UnitTestDemo;

/**
 * Created by craig on 7/7/17.
 */
public class StringObject {
    public String str;
    public StringObject(String str){ this.str = str; }
    public String toString(){ return str; }

    public void swap(StringObject a){
        if(a == null)
            throw new NullPointerException("Parameters can't be null");

        String t = a.str;
        a.str = this.str;
        this.str = t;
    }
    public static void main(String[] args){
        StringObject x = new StringObject("hello");
        StringObject y = new StringObject("world");
        x.swap(y);
        System.out.println(x);
        System.out.println(y);
    }
}
