import se450.week1.UnitTestDemo.StringObject;

public class Main {
	public static void main(String[] args){ 
		StringObject x = new StringObject("hello");
		StringObject y = new StringObject("world"); 
		x.swap(y); 
		System.out.println(x); 
		System.out.println(y); 
	}
}

