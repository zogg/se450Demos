package se450.week4.CompositeDemo;

public class FileSystemFactory {
	public static IFileSystemItem getFileSystem() throws Exception{
		
		FileSystemBuilder builder = new FileSystemBuilder("root");
		
		builder.addFolderToParent("root", "Folder1");
		builder.addFileToParent("root", 56, "File3");
		
		builder.addFileToParent("Folder1", 63, "File2");
		builder.addFolderToParent("Folder1", "Folder2");
		
		builder.addFileToParent("Folder2", 45, "File1");
		
		IFileSystemItem fileSystem = builder.getFileSystem();
		
		return fileSystem;
		
		/*File file1 = new File(45, "File1");
		Folder folder2 = new Folder("Folder2");
		folder2.add(file1);
		
		File file2 = new File(63, "File2");
		Folder folder1 = new Folder("Folder1");
		
		folder1.add(file2);
		folder1.add(folder2);
		
		File file3 = new File(56, "File3");
		
		Folder root = new Folder("Root");
		
		root.add(file3);
		root.add(folder1);
		
		return root;*/
	}
}
