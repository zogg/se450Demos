package se450.week4.CompositeDemo;

public class File implements IFileSystemItem {
	private int size;
	private String name;
	
	public File(int size, String name){
		this.size = size;
		this.name = name;
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public IFileSystemItem find(String itemName) {
		if(name.equals(itemName))
			return this;
		
		return null;
	}
}
