package se450.week4.CompositeDemo;

public interface IFileSystemItem {
	int getSize();

	IFileSystemItem find(String itemName);
}
