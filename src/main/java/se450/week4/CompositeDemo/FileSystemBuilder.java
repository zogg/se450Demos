package se450.week4.CompositeDemo;

public class FileSystemBuilder {
	private Folder rootFolder;
	
	public FileSystemBuilder(String rootName){
		rootFolder = new Folder(rootName);
	}

	public IFileSystemItem getFileSystem() {
		return rootFolder;
	}

	public void addFolderToParent(String parentName, String folderName) throws Exception {
		Folder newFolder = new Folder(folderName);
		addFileSystemItemToParent(newFolder, parentName);
	}

	public void addFileToParent(String parentName, int fileSize, String fileName) throws Exception {
		File newFile = new File(fileSize, fileName);
		addFileSystemItemToParent(newFile, parentName);
	}

	private void addFileSystemItemToParent(IFileSystemItem newFileSystemItem, String parentName) throws Exception {
		IFileSystemItem parentFolder = rootFolder.find(parentName);
		
		if(parentFolder == null || !(parentFolder instanceof Folder))
			throw new Exception("Folder not found");
		
		((Folder)parentFolder).add(newFileSystemItem);
	}
}
