package se450.week4.CompositeDemo;

public class Main {

	public static void main(String[] args) throws Exception {
		IFileSystemItem fileSystem = FileSystemFactory.getFileSystem();
		int result = fileSystem.getSize();
		System.out.println(result);
	}

}
