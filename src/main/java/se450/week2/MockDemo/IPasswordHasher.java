package se450.week2.MockDemo;

public interface IPasswordHasher {
	String getPasswordHash(String password); 
}
