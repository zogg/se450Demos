package se450.week2.MockDemo;

public interface IPasswordValidator {
	boolean isPasswordValid(User user, String password);
}
