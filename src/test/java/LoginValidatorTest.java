import static org.junit.Assert.*;

import org.junit.Test;
import se450.week2.MockDemo.LoginValidator;
import se450.week2.MockDemo.User;

public class LoginValidatorTest {

	@Test
	public void testValidateLoginDoesntThrowExceptionForValidPassword() {
		// Arrange
		User user = new User();
		user.passwordHash = "testHash";
		LoginValidator lv = new LoginValidator((User u, String password) -> true);
		
		// Act
		try {
			lv.validateLogin(user, "password");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			fail();
		}
		
		// Assert
		assertTrue(true);
	}
	
	@Test
	public void testValidateLoginThrowsExceptionForInvalidPassword() {
		// Arrange
		User user = new User();
		user.passwordHash = "testHash";
		LoginValidator lv = new LoginValidator((User u, String password) -> false);
		
		// Act
		try {
			lv.validateLogin(user, "password");
			fail();
		} catch (Exception e) {
		}
		
		// Assert
		assertTrue(true);
	}

}
