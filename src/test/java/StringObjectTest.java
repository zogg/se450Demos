import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import se450.week1.UnitTestDemo.StringObject;

public class StringObjectTest {
	StringObject a;
	StringObject b;
	
	@Before
	public void testSetup(){
		a = new StringObject("Hello");
		b = new StringObject("World");
	}
	
	@Test
	public void testSwap(){
		// Arrange
		
		// Act
		a.swap(b);
		
		// Assert
		assertEquals(a.str, "World");
		assertEquals(b.str, "Hello");
	}
	
	@Test
	public void testSwapWithNullParameters(){
		// Arrange		
		
		try{
			// Act
			a.swap(null);
			fail();
		} catch(NullPointerException ex) {
			// Assert
			assertEquals(ex.getMessage(), "Parameters can't be null");
		}
	}
	
	@Test
	public void testSwapWithNullStringValues(){
		// Arrange
		StringObject a = new StringObject("Hello");
		StringObject b = new StringObject(null);
		
		// Act
		a.swap(b);
		
		// Assert
		assertEquals(a.str, null);
		assertEquals(b.str, "Hello");
	}
}
