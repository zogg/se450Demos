import org.junit.Test;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import se450.midterm.UnitTestDemo.VideoObj;

/**
 * Created by craig on 7/7/17.
 */
public class VideoObjTest {
    @Test
    public void testDirectorIsEmptyStringShouldThrowIllegalArgumentException(){
        try {
            VideoObj obj = new VideoObj("", 1234, "");
            fail();
        } catch(IllegalArgumentException ex) {
            assertEquals("Director", ex.getMessage());
        }
    }

    @Test
    public void testDirectorGetsSetValidValue(){
        VideoObj obj = new VideoObj("", 1234, "Ed Wood");
        assertEquals("Ed Wood", obj.getDirector());
    }
}
